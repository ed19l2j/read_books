from flask_wtf import FlaskForm
from wtforms import IntegerField, DateField, TextAreaField, BooleanField, StringField
from wtforms.validators import DataRequired

# Form used for the login page and only requires username and password
class Login(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])

# Signup form which demands a username password firstname and email as username and email must be unique
class Signup(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    first_name = StringField('first_name', validators=[DataRequired()])
    last_name = StringField('last_name')
    email = StringField('email', validators=[DataRequired()])
    phone_number = StringField('phone_number')
    DOB = DateField('first_name')

# Stores a USers session so they stay logged into until they log out
class SessionForm(FlaskForm):
    value = StringField('value', validators=[DataRequired()])

# For a User to input a new book they have read and it be linked to their profile
class NewBook(FlaskForm):
    title = StringField('title', validators=[DataRequired()])
    author = StringField('author', validators=[DataRequired()])

# For a user to input their existing details so they can update their password
class ChangePassword(FlaskForm):
    username = StringField('title', validators=[DataRequired()])
    password = StringField('author', validators=[DataRequired()])
    new_password = StringField('author', validators=[DataRequired()])
