from flask import render_template, flash, session, url_for, request, redirect
from app import app, db, models
from .forms import Login, SessionForm, Signup, NewBook, ChangePassword

# Main login page. All other pages except signup get redirected here if User isnt logged in
@app.route('/', methods=['GET', 'POST'])
def login():
    form = Login()
    if form.validate_on_submit():
        flash('Succesfully received form data.')
        for p in models.User.query.all():
            if p.username == form.username.data and p.password == form.password.data:
                session['variable'] = form.username.data
                return redirect(url_for('read_books'))
    return render_template('login.html',
                           title='login',
                           form=form)


# Main login page. All other pages except signup get redirected here if User isnt logged in
@app.route('/change_password', methods=['GET', 'POST'])
def change_password():
    form = ChangePassword()
    if form.validate_on_submit():
        flash('Succesfully received form data.')
        for p in models.User.query.all():
            if p.username == form.username.data and p.password == form.password.data:
                p.password = form.new_password.data
                db.session.commit()
                return redirect(url_for('login'))
    return render_template('change_password.html',
                           title='change_password',
                           form=form)

# Allows a user to signout with pops the session that was being stored for them
@app.route('/signout')
def signout():
	session.pop('variable', None)
	return redirect(url_for('login'))

# Allows a user to create an account with they can then login with
@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = Signup()
    if form.validate_on_submit():
        flash('Succesfully received form data.')
        isUsernameUnique = True
        for p in models.User.query.all():
            if p.username == form.username.data:
                    isUsernameUnique = False
        if isUsernameUnique == True:
            p = models.User(first_name=form.first_name.data,last_name=form.last_name.data,
            email=form.email.data,phone_number=form.phone_number.data,DOB=form.DOB.data,
            username=form.username.data,password=form.password.data)
            db.session.add(p)
            db.session.commit()
            return redirect(url_for('login'))
    return render_template('signup.html',
                           title='signup',
                           form=form)

# Allows Users to see all the books they have read so far that are linked to that account
@app.route('/read_books', methods=['GET', 'POST'])
def read_books():
    if 'variable' in session:
        variable = session['variable']
        app.logger.info('index route request')
        for u in models.User.query.all():
            if u.username == variable:
                theuser = u
                return render_template('read_books.html',
                                    title='Read Books',
                                    books=u.books,
                                    user=variable)
        return render_template('read_books.html',
                            title='Read Books',
                            user=variable)
    return redirect(url_for('login'))

# Shows the User their own profile
@app.route('/profile', methods=['GET', 'POST'])
def profile():
    if 'variable' in session:
        variable = session['variable']
        app.logger.info('index route request')
        for u in models.User.query.all():
            if u.username == variable:
                booksread = 0
                for book in u.books:
                    booksread = booksread + 1
                return render_template('profile.html',
                                    title='Profile',
                                    profile=u,
                                    user=variable,
                                    booksread=booksread)
    return redirect(url_for('login'))

# Allows a User to add a new book they have read to their account
@app.route('/new_book', methods=['GET', 'POST'])
def new_book():
    form = NewBook()
    if 'variable' in session:
        variable = session['variable']
        app.logger.info('index route request')
        if form.validate_on_submit():
            flash('Succesfully received form data.')
            for u in models.User.query.all():
                if u.username == variable:
                    theuser = u
                    thebook = models.Book(title=form.title.data,author=form.author.data)
                    thebook.user = theuser
                    db.session.add(thebook)
                    db.session.commit()
            return redirect(url_for('read_books'))
        return render_template('new_book.html',
                            title='New Book',
                            form=form,
                            user=variable)
    return redirect(url_for('login'))
