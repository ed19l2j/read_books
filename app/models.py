from app import db

# One to many relationship between User and Book as a User can have many Books 
class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String(100), index=True)
    title = db.Column(db.String(100), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    def __repr__(self):
        return '{}{}{}'.format(self.id, self.author, self.title)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), index=True, unique=True)
    password = db.Column(db.String(30), index=True)
    first_name = db.Column(db.String(100), index=True)
    last_name = db.Column(db.String(100), index=True)
    email = db.Column(db.String(100), index=True, unique=True)
    phone_number = db.Column(db.String(20) ,index=True)
    DOB = db.Column(db.DateTime)
    am_of_books_read = db.Column(db.Integer)
    books = db.relationship('Book', backref='user', lazy='dynamic')
    def __repr__(self):
        return '{}{}{}{}{}{}{}{}{}'.format(self.id, self.username, self.password, self.first_name, self.last_name, self.email, self.phone_number, self.DOB, self.am_of_books_read)
